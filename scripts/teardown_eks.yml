- hosts: localhost
  gather_facts: false
  connection: local
  vars:
    ansible_python_interpreter: '{{ ansible_playbook_python }}'
    aws_region: us-west-2
    # https://us-east-1.console.aws.amazon.com/route53/v2/hostedzones?region=us-west-2#ListRecordSets/Z2CBLNEAFNYJK4
    openraven_io_r53_zone_id: Z2CBLNEAFNYJK4

  vars_prompt:
  - name: org_slug
    private: false
    prompt: the OrgSlug?

  tasks:
  - name: find its VPC
    amazon.aws.ec2_vpc_net_info:
      filters:
        tag:OrgSlug: '{{ org_slug }}'
    register: find_vpc

  - name: ensure there is only one and is what we asked for
    assert:
      msg: expected to find only one VPC and for it to have our OrgSlug Tag
      that:
      - (find_vpc.vpcs | length) == 1
      - (find_vpc.vpcs | first).tags.OrgSlug == org_slug

  - name: promote _our_ VPC to a fact
    vars:
      the_vpc0: '{{ find_vpc.vpcs | first }}'
    set_fact:
      the_vpc: '{{ the_vpc0 }}'
      the_vpc_id: '{{ the_vpc0.vpc_id }}'
      OpenRavenGroupId: '{{ the_vpc0.tags.OpenRavenGroupId }}'

  - name: grab the top-level stack info so we can nuke its Splunk bucket
    amazon.aws.cloudformation_info:
      stack_name: orvn-{{ org_slug }}
    register: top_stack_info

  - name: promote the top-stack info
    vars:
      the_stack: '{{ top_stack_info.cloudformation["orvn-"+org_slug] }}'
    set_fact:
      top_stack: '{{ the_stack }}'
      splunk_bucket: '{{ the_stack.stack_outputs.SplunkBucket }}'
      deploy_channel: '{{ the_stack.stack_parameters.DeployChannel }}'
        

  - name: see if there is a ClusterParam stack
    # sorry, it seems the stacks have different suffixes
    with_items:
    - ClusterParam13
    - ParamGroup13
    amazon.aws.cloudformation_info:
      stack_name: orvn-{{ org_slug }}-{{ item }}
    register: maybe_param_stack

  - name: promote any discovered ClusterParam stack
    set_fact:
      cluster_param_stack_names: >-
        {{ maybe_param_stack.results
        | map(attribute="cloudformation")
        | selectattr("stack_name", "boolean")
        | map(attribute="stack_name") | list }}

  - name: steps for that stack info, if applicable
    when: cluster_param_stack_names|length > 0
    block:
    - name: PRINT OUT that ClusterParam stack, if applicable
      debug:
        msg: you will want to $(rain rm -y {{cluster_param_stack_names | join("\n") }}) if that information looks real to you

    - name: pause for display of that stack info
      ansible.builtin.wait_for:
        timeout: 15

  - name: find its provisioned ELBs
    # community.aws.ec2_elb_info:
    # fuck that ^^^:     "msg": "boto required for this module"
    # it has no "filters:" so we have to post-facto filter them
    command: >-
      aws elb describe-load-balancers --query {{ jq | quote }} --output text
    vars:
      jq: >-
        join(`\n`, LoadBalancerDescriptions[?VPCId == '{{ the_vpc_id }}'].LoadBalancerName)
    # failed_when:  my_elb_cmd.stdout|length == 0 or my_elb_cmd.stderr_lines|length > 0
    register: my_elb_cmd

  - name: nuke the {{ item }} ELB, if required
    when: my_elb_cmd.stdout|length > 0
    loop: '{{ my_elb_cmd.stdout_lines | map("trim") | list }}'
    command: aws elb delete-load-balancer --load-balancer-name {{ item | quote }}

  - name: find the SGs in my VPC
    amazon.aws.ec2_security_group_info:
      filters:
        vpc-id: '{{ the_vpc.vpc_id }}'
    register: my_sgs

  - name: flush the SG ingress rules (well, actually, all of them)
    loop: '{{ my_sgs.security_groups }}'
    loop_control:
      label: '{{ item.group_name }}'
    retries: 3
    # see teardown_eks.kaboom.log for an example of this
    failed_when: ("DependencyViolation" == (flush_sgs.error|d({"code":""})).code)
    register: flush_sgs
    # it seems that DependencyViolation doesn't count as a _failure_ it is an _error_
    # because the underlying python explodes
    ignore_errors: true
    amazon.aws.ec2_security_group:
      group_id: '{{ item.group_id }}'
      name: '{{ item.group_name }}'
      description: '{{ item.description }}'
      vpc_id: '{{ the_vpc_id }}'
      # state: '{{ "absent" if ("Kubernetes ELB " in item.description) else "present" }}'
      purge_rules: true
      rules: []

  - name: show it
    debug:
      var: flush_sgs

  - name: stall 30s for eventual consistency
    wait_for:
      timeout: 30

  - name: now delete the non-default SGs
    loop: '{{ my_sgs.security_groups | rejectattr("group_name", "eq", "default") }}'
    loop_control:
      label: '{{ item.group_name }}'
    retries: 3
    register: delete_sgs
    # this is required because it's an *exception*
    ignore_errors: yes
    # but this triggers the retries: logic because it promotes the Exception to a Failure
    # negative, it turns out there are no "results" yet: failed_when: (delete_sgs.results | selectattr("exception", "string") | length) > 0
    failed_when: (delete_sgs.exception | d("", True) | length) > 0
    amazon.aws.ec2_security_group:
      group_id: '{{ item.group_id }}'
      name: '{{ item.group_name }}'
      description: '{{ item.description }}'
      vpc_id: '{{ the_vpc_id }}'
      state: absent

  - name: show it
    debug:
      var: delete_sgs

  - name: clean up their volumes
    amazon.aws.ec2_vol_info:
      filters:
        tag:KubernetesCluster: orvn-{{ OpenRavenGroupId | mandatory }}
        # maybe?
        # tag:kubernetes.io/cluster/orvn-00gCafebabe: owned
        status: available
    register: their_vols

  - name: purging their volumes
    loop: '{{ their_vols.volumes }}'
    loop_control:
      label: '{{ item.id }}'
    amazon.aws.ec2_vol:
      id: '{{ item.id }}'
      state: absent

  - name: find their snaps
    amazon.aws.ec2_snapshot_info:
      filters:
        status: completed
        # maybe? ebs.csi.aws.com/cluster=true
        # sorry, the snaps don't get KubernetesCluster: orvn so we have to use this
        tag-key: '{{ "kubernetes.io/cluster/orvn-"+OpenRavenGroupId }}'
    register: their_snaps

  - name: purge those snaps
    amazon.aws.ec2_snapshot:
      snapshot_id: '{{ item.snapshot_id }}'
      state: absent
    loop: '{{ their_snaps.snapshots }}'
    loop_control:
      label: '{{ item.snapshot_id }}'

  - name: cook Prod credentials for DNS monkeying
    command: aws-vault exec --json --duration 1h --region {{ aws_region }} prod
    environment:
      # we have to mask off the outer vault or it whines
      AWS_VAULT: ''
    register: aws_prod_creds_cmd

  - name: promote Prod creds
    set_fact:
      aws_prod_creds: '{{ aws_prod_creds_cmd.stdout }}'
      # - AccessKeyId
      # - Expiration
      # - SecretAccessKey
      # - SessionToken
      # - Version

  - name: nuke its CNAME
    amazon.aws.route53:
      state: absent
      hosted_zone_id: '{{ openraven_io_r53_zone_id }}'
      record: '{{ org_slug }}.openraven.io.'
      type: CNAME
      # the fine manual says value and ttl are required but :four_leaf_clover:
      access_key: '{{ aws_prod_creds.AccessKeyId }}'
      secret_key: '{{ aws_prod_creds.SecretAccessKey }}'
      session_token: '{{ aws_prod_creds.SessionToken }}'
    ignore_errors: true
    register: r53_cleanup

  - name: show the R53 outcome
    debug:
      var: r53_cleanup

  - name: delete the SplunkBucket if saas-staging
    # otherwise it'll need some rm --recursive and please wait
    when: the_vpc.owner_id == "564255997539"
    command: aws s3api delete-bucket --bucket {{ splunk_bucket }}
    # on success it emits nothing

  - name: show the rain command to finish the teardown
    debug:
      msg: rain rm -y orvn-{{ org_slug }}
