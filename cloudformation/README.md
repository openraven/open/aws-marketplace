# WHAT IN THE WORLD!?

So, thanks to these absolutely nonsensical issues (
[1](https://github.com/aws/containers-roadmap/issues/185),
[2](https://github.com/aws/containers-roadmap/issues/378),
[3](https://github.com/aws/containers-roadmap/issues/554)),
we need a way to configure EKS IAM auth in a declarative way
after cluster creation otherwise we cannot inject the `helmfile0` pod and it just sits there burning money

We started out using [`AWSQS::EKS::Cluster`](https://github.com/aws-quickstart/quickstart-amazon-eks-cluster-resource-provider#readme)
which works fine for initial provisioning, but doesn't survive `aws cloudformation update-stack` which makes it less valuable

One may think that [`AWSQS::Kubernetes::Resource`](https://github.com/aws-quickstart/quickstart-kubernetes-resource-provider#readme)
would get it done, but its IAM Role is one-hop, meaning the whole stack would have to come alive using it,
otherwise the `AWS::EKS::Cluster` would be owned by another Role and we're right back where we started from.
There is [an open issue](https://github.com/aws-quickstart/quickstart-kubernetes-resource-provider/issues/42) 
to teach it to `AssumeRole` but that's not today

The [`eks-aws-auth-configmap-api`](https://github.com/aws-samples/eks-aws-auth-configmap#readme) is very close to what we want
but [is hard-coded demo code](https://github.com/aws-samples/eks-aws-auth-configmap/blob/136111457463/lambda/lambda_function.py#L8-L16)
and doesn't integrate with CloudFormation anyway

The [`eks-auth-configmap.yaml`](./eks-auth-configmap.yaml) is some trickery that uses `Fn::Transform:`
to "side-load" some Lambda execution into the stack, and it works, but is very opaque

In the end, we decided to just bind the `AWSQS::Kubernetes::Resource` type activation to an existing IAM Role,
versus creating a new Role with all the permissions they request in GitHub, and force all stacks to come up via the lambda

# What Are These Files?
* plumbing bits:
  * `awsqs-kubernetes-resource.yaml` -- binds `AWSQS::Kubernetes::Resource` to the Region as the cited Role
  * `awsqs-kubernetes-helm.yaml` -- binds `AWSQS::Kubernetes::Helm` to the Region as a Role used just for that purpose. One will find a reference to that Role in the `AssumeRolePolicyDocument` of the IAM Role that helmfile0 runs under. Sekurity!

* `saas.yaml` -- the CloudFormation stack that installs Open Raven
  * `stack-vpc.yaml` -- the substack that provisions the VPC into which RDS and EKS will live
  * `rds.yaml` -- the substack that provisions the RDS into that VPC (there's actually also an `rds-lite.yaml` for when you don't want to wake the for-damn-ever it takes RDS to provision)
  * `eks1a.yaml` -- the substack that brings up EKS proper
  * `eks1b.yaml` -- the substack that writes our `aws-auth` `ConfigMap` and then injects the `helmfile0` Pod into EKS, kicking off the formal install

* `stack.iam.yaml` -- the IAM Policy required to run `cloudformation:CreateStack` on the `saas.yaml` template; very, very likely it is wider than it should be, so MRs are welcome
  * `stack.iam.json` -- in some sense, this should not be in source control, since it is merely the `yaml2json` version of `stack.iam.yaml` but here we are
* `openraven-discovery-role.yaml` -- the policy customers deploy either in individual accounts or via StackSet to enable discovery
