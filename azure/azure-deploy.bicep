targetScope = 'resourceGroup'

@description('the Open Raven GroupId')
param groupId string

@description('the OIDC "iss", starting with https://')
param oidcIssuer string

@description('the k8s service account name')
param serviceAccountName string = 'azure-discovery-svc'

@description('The Azure location of the new managed identity')
param location string = resourceGroup().location

var identityName = 'orvn-${groupId}-cross-account'

// just to prevent collisions during development
var subDeploySuffix = guid(subscription().subscriptionId, groupId)

// it seems to be picky about using .properties.audiences so just cheat and copy-paste it
var umiAudience = 'api://AzureADTokenExchange'

resource identity 'Microsoft.ManagedIdentity/userAssignedIdentities@2023-01-31' = {
  name: identityName
  location: location
  tags: {
    project: 'openraven'
  }
}

resource identityName_openraven_oidc1 'Microsoft.ManagedIdentity/userAssignedIdentities/federatedIdentityCredentials@2023-01-31' = {
  parent: identity
  // this name is scoped to the UMI and thus can be the same across deployments
  name: 'openraven-oidc-aws'
  properties: {
    issuer: oidcIssuer
    subject: 'system:serviceaccount:aws:${serviceAccountName}'
    // see: https://github.com/Azure/azure-workload-identity/blob/v1.2.0/docs/book/src/topics/federated-identity-credential.md#federated-identity-credential-for-an-azure-ad-application
    audiences: [
      umiAudience
    ]
  }
}

resource identityName_openraven_oidc2 'Microsoft.ManagedIdentity/userAssignedIdentities/federatedIdentityCredentials@2023-01-31' = {
  /*
    You may wonder why any sane person would do this nonsense and it's because Azure is too awesome for us:

    Too many Federated Identity Credentials are written concurrently for the managed identity
    '/subscriptions/.../providers/microsoft.managedidentity/userassignedidentities/orvn-00gcafebabe-cross-account'.
    Concurrent Federated Identity Credentials writes under the same managed identity are not supported.
    (Code: ConcurrentFederatedIdentityCredentialsWritesForSingleManagedIdentity)


  I also tried trickery using dependsOn in a [for] expression but the items are not allowed to reference their own resource name,
  even in prior iterations
  */
  parent: identity
  name: 'openraven-oidc-dmap'
  properties: {
    issuer: identityName_openraven_oidc1.properties.issuer
    subject: 'system:serviceaccount:dmap:${serviceAccountName}'
    audiences: [
      umiAudience
    ]
  }
}

resource identityName_openraven_oidc3 'Microsoft.ManagedIdentity/userAssignedIdentities/federatedIdentityCredentials@2023-01-31' = {
  parent: identity
  name: 'openraven-oidc-ui'
  properties: {
    issuer: identityName_openraven_oidc2.properties.issuer
    subject: 'system:serviceaccount:ui:${serviceAccountName}'
    audiences: [
      umiAudience
    ]
  }
}


module umiSub './userManagedIdentitySub.bicep' = {
    scope: subscription()
    name: 'orvn-${groupId}-${subDeploySuffix}'
    params: {
        userManagedIdentityId: identity.properties.principalId
    }
}

/*
module umiTenant './userManagedIdentityTenant.bicep' = {
this needs TLC since even Global Administrator cannot bind it without elevating themselves first
}
*/

@description('the Tenant Id, emitted here for convenience')
output azureTenantId string = tenant().tenantId

@description('the Client Id of the created User Managed Identity')
output azureClientId string = identity.properties.clientId

@description('the Subscription Id, emitted here for convenience')
output azureSubscriptionId string = subscription().subscriptionId

@description('the URI in the cluster for pasting the values back into the onboarding modal')
output returnUri string = '/source/azure/accounts?tId=${tenant().tenantId}&sId=${subscription().subscriptionId}&cId=${identity.properties.clientId}&pId=${identity.properties.principalId}'

@description('the Principal Id of the created User Managed Identity')
output userManagedIdentityId string = identity.properties.principalId
