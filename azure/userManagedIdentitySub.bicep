targetScope = 'subscription'

@description('the U.M.I. **principalId**')
param userManagedIdentityId string

/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles#reader */
var Role_Reader_Id = 'acdd72a7-3385-48ef-bd42-f606fba81ae7'

/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#backup-reader */
var Role_Backup_Reader_Id = 'a795c7a0-d4a2-40c1-ae25-d81f01202912'
/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#data-box-reader */
var Role_Data_Box_Reader_Id = '028f4ed7-e2a9-465e-a8f4-9c0ffdfdc027'
/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#elastic-san-reader */
var Role_Elastic_SAN_Reader_Id = 'af6a70f8-3c9f-4105-acf1-d719e9fca4ca'
/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#reader-and-data-access */
var Role_Reader_and_Data_Access_Id = 'c12c1c16-33a1-487b-954d-41c89c60f349'
/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#storage-blob-data-reader */
var Role_Storage_Blob_Data_Reader_Id = '2a2b9908-6ea1-4ae2-8e65-a410df84e7d1'
/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#storage-file-data-privileged-reader */
var Role_Storage_File_Data_Privileged_Reader_Id = 'b8eda974-7b85-4f76-af95-65846b26df6d'
/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#storage-file-data-smb-share-reader */
var Role_Storage_File_Data_SMB_Share_Reader_Id = 'aba4ae5f-2193-4029-9191-0cb91df5e314'
/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#storage-queue-data-reader */
var Role_Storage_Queue_Data_Reader_Id = '19e7f393-937e-4f77-808e-94535e297925'
/** https://learn.microsoft.com/en-us/azure/role-based-access-control/built-in-roles/storage#storage-table-data-reader */
var Role_Storage_Table_Data_Reader_Id = '76199698-9eea-4c19-bc75-cec21354c6b6'

var RoleIds = [
  Role_Reader_Id
  Role_Backup_Reader_Id
  Role_Data_Box_Reader_Id
  Role_Elastic_SAN_Reader_Id
  Role_Reader_and_Data_Access_Id
  Role_Storage_Blob_Data_Reader_Id
  Role_Storage_File_Data_Privileged_Reader_Id
  Role_Storage_File_Data_SMB_Share_Reader_Id
  Role_Storage_Queue_Data_Reader_Id
  Role_Storage_Table_Data_Reader_Id
]

resource conInstRole 'Microsoft.Authorization/roleDefinitions@2022-04-01' = {
  name: guid(tenant().tenantId, subscription().subscriptionId, userManagedIdentityId, 'conInstRole')
  scope: subscription()
  properties: {
    roleName: 'orvn-${userManagedIdentityId}'
    type: 'customRole'
    description: 'allow the Open Raven managed identity to create/remove Container Instances'
    assignableScopes: [
      subscription().id
    ]
    permissions: [
      {
        actions: [
          'Microsoft.ContainerInstance/containerGroups/write'
          'Microsoft.ContainerInstance/containerGroups/delete'
          'Microsoft.Resources/subscriptions/resourceGroups/write'
          'Microsoft.Resources/subscriptions/resourceGroups/delete'
        ]
      }
    ]
  }
}

resource assignRoles 'Microsoft.Authorization/roleAssignments@2022-04-01' = [for id in RoleIds : {
  scope: subscription()
  name: guid(tenant().tenantId, subscription().subscriptionId, userManagedIdentityId, id)
  properties: {
    description: 'allow Open Raven to read Azure resources in your Tenant'
    principalId: userManagedIdentityId
    roleDefinitionId: tenantResourceId('Microsoft.Authorization/roleDefinitions', id)
    principalType: 'ServicePrincipal'
  }
}]

resource assignMyRole 'Microsoft.Authorization/roleAssignments@2022-04-01' = {
    scope: subscription()
    name: guid(tenant().tenantId, subscription().subscriptionId, userManagedIdentityId, conInstRole.id)
    properties: {
        description: 'allow Open Raven to create/remove Container Instances'
        principalId: userManagedIdentityId
        roleDefinitionId: conInstRole.id
        principalType: 'ServicePrincipal'
    }
}

// there are no outputs for this, we already have all that we need
