targetScope = 'tenant'

@description('the U.M.I. **principalId**')
param userManagedIdentityId string

/** https://learn.microsoft.com/en-us/entra/identity/role-based-access-control/permissions-reference#global-reader */
var BuiltInRole_Global_Reader_Id = 'f2ef992c-3afb-46b9-b7cf-a126ee74c451'

// no, this is an Entra gizmo and thus Azure cannot create it :sob:
// resource groupForRoleAssignment 'Microsoft.Group' ...

resource assignGlobalReaderRole 'Microsoft.Authorization/roleAssignments@2022-04-01' = {
  scope: tenant()
  name: guid(tenant().tenantId, userManagedIdentityId,  BuiltInRole_Global_Reader_Id)
  properties: {
    description: 'allow Open Raven to read Azure resources in your Tenant'
    principalId: userManagedIdentityId
    roleDefinitionId: BuiltInRole_Global_Reader_Id
    // yes, but one will have to create this Group manually for now
    principalType: 'Group'
  }
}

// there are no outputs for this, we already have all that we need
