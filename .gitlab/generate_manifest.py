#!/usr/bin/env python3
import json
import os
import sys
import hashlib
from datetime import datetime

file_list = sys.argv[1:]

manifest = {"files": []}
for file_name in file_list:
    with open(file_name, "rb") as f:
        sha256 = hashlib.sha256(f.read()).hexdigest()

    manifest["files"].append({"name": file_name, "sha256": sha256})

manifest['git_ref'] = os.environ['CI_COMMIT_SHA']
manifest['timestamp'] = datetime.now(tz=None).isoformat()
manifest['build_id'] = os.environ['CI_PIPELINE_ID']

with open("bootstrap_aws_manifest.json", "w") as f:
    json.dump(manifest, f)
