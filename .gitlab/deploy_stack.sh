#!/usr/bin/env bash
set -euo pipefail
TRACE="${TRACE:-}"
if [[ -n "$TRACE" ]]; then
  set -x
fi

my_dir=$(dirname "$0")
"$my_dir"/_install_bicep.sh

# be aware this reformats and git-brands the files 
"$my_dir"/check_cfn.sh

cd cloudformation

# Avoid doing processing on the filenames
# shellcheck disable=SC2035
../.gitlab/generate_manifest.py *.json *.template *.yaml

for f in *.json *.template *.yaml; do
    aws s3 cp "${f}" "s3://${HELM_S3_BUCKET}/${f}"
done

# Processing Bicep Files for Azure
cd ../azure

for x in *.bicep; do
    [[ -e "$x" ]] || continue
    bicep build "$x"
done

for f in *.json *.bicep; do
    [[ -e "$f" ]] || continue
    aws s3 cp "${f}" "s3://${HELM_S3_BUCKET}/${f}"
done

chat_start
echo "Deploying to ${HELM_S3_BUCKET}"
chat_stop
