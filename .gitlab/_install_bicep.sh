#!/usr/bin/env bash
set -euo pipefail
TRACE="${TRACE:-}"
if [[ -n "$TRACE" ]]; then
  set -x
fi

case "$CI_RUNNER_EXECUTABLE_ARCH" in
  *amd64*)
    suffix=x64
  ;;
  *arm64*)
    suffix=arm64
  ;;
  *)
    echo "Unsupported architecture: \"$CI_RUNNER_EXECUTABLE_ARCH\"" >&2
    exit 1
  ;;
esac

mkdir -p "$XDG_CACHE_HOME"

bicep_exe="$XDG_CACHE_HOME/bicep-${BICEP_VERSION}-${suffix}"
if [[ ! -x      "$bicep_exe" ]]; then
    curl -fsSLo "$bicep_exe" \
      "https://github.com/Azure/bicep/releases/download/v${BICEP_VERSION}/bicep-linux-${suffix}"
    chmod a+x   "$bicep_exe"
fi
ln -svf "$bicep_exe" /usr/bin/bicep
