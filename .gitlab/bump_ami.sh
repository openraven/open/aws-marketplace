#! /usr/bin/env bash
set -euo pipefail
[[ -n "${TRACE:-}" ]] && set -x
# of the form 2222.3.4
release_tag="$1"
packer_vars_fn=ami/vars.pkr.hcl
old_ami_url=$(grep -o 'https://.*flatcar_production_ami.*' "$packer_vars_fn")
existing_release=$(echo "$old_ami_url" | sed -nEe 's|.*/([0-9.]+)/flatcar_production_ami.*|\1|p')
new_ami_url=$(echo "$old_ami_url" | sed -e "s|$existing_release|$release_tag|")
image_id=$(curl -fsL "$new_ami_url")
sed -i"" -e 's|"ami-[^"]*|"'"$image_id|; s|$existing_release|$release_tag|g" "$packer_vars_fn"
