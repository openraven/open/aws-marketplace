#! /usr/bin/env python3
# coding=utf-8
import json
import sys

import yaml
from botocore.session import get_session

input_fn = sys.argv[1]
output_fn = sys.argv[2]

with open(input_fn) as fh:
    m = json.load(fh)
a_by_region = {}
b_list = m['builds']
for build in b_list:
    aid_csv = build['artifact_id']
    aid_list = aid_csv.split(',')
    for aid in aid_list:
        region, ami_id = aid.split(':')
        a_by_region[region] = {'ImageId': ami_id}

sess = get_session()
for r, ami_info in a_by_region.items():
    aid = ami_info['ImageId']
    print('Looking up "{}" in "{}"'.format(aid, r),
          file=sys.stderr)
    ec2 = sess.create_client('ec2', region_name=r)
    di_resp = ec2.describe_images(ImageIds=[aid])
    for img in di_resp['Images']:
        ebs_blocks = [bdm for bdm in img['BlockDeviceMappings']
                      if 'Ebs' in bdm]
        for bdm in ebs_blocks:
            snap_id = bdm['Ebs']['SnapshotId']
            ami_info['SnapshotId'] = snap_id
results = {
    "Mappings": {
        "AmiAndSnapByRegion": a_by_region
    }
}

with open(output_fn, 'w') as fh:
    yaml.safe_dump(results, fh)
