# https://us-east-1.console.aws.amazon.com/ec2/v2/home?region=us-east-1#Images:visibility=public-images;ownerAlias=075585003325;creationDate=%3E2021-03-01T00:00-07:00;name=stable;virtualizationType=hvm;sort=desc:name
# https://stable.release.flatcar-linux.net/amd64-usr/2765.2.3/flatcar_production_ami_us-east-1.txt
# 075585003325/Flatcar-stable-2765.2.3-hvm
variable "ami_id" {
  type    = string
  default = "ami-0eb8222969c04150e"
}

# watch out, omitting the regions list just causes it to create the AMI in the **current** region
variable "ami_regions" {
  type    = list(string)
  default = [
    "us-east-1",
  ]
}

# should be either empty, which is a private AMI, or "all" meaning to make it public
variable "ami_groups" {
  type    = list(string)
  default = [
    "all",
  ]
}

variable "instance_type" {
  type    = string
  default = "t3.small"
}

# is used to name-mangle the resulting AMI without otherwise fiddling with the release_id
variable "ami_extra" {
  type    = string
  default = ""
}

# the region in which to launch the EC2 instance used to snapshot the EBS volume
variable "region" {
  type    = string
  default = "us-east-1"
}

variable "release_id" {
  type    = string
  default = ""
}

variable "sg_ids" {
  type    = list(string)
  # "us-east-1-default"
  default = [
    "sg-032418196f60e40c3",
  ]
}

variable "ssh_username" {
  type    = string
  default = "core"
}

variable "subnet_id" {
  type    = string
  # "public-east-1a"
  default = "subnet-0dbfa4e143e2dc25f"
}

# be careful, setting that **forces** the minimum size;
# we also configure the volume size in CFN (granted, 32GB may very well be reasonable
# but is also artificially limiting in how the AMI can be used)
variable "volume_size" {
  type    = string
  default = "16"
}

variable "vpc_id" {
  type    = string
  #  "us-east-1"
  default = "vpc-024bd0fb6975dc249"
}
